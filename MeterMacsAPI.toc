\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\contentsline {section}{\numberline {2}API Details}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}Message Queue}{2}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Using the Message Queue}{2}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Finding Data in MeterMACS}{3}{subsection.2.3}
\contentsline {section}{\numberline {3}Available Message Types}{4}{section.3}
\contentsline {subsection}{\numberline {3.1}Customer Specific Requests}{4}{subsection.3.1}
\contentsline {subsubsection}{\numberline {3.1.1}Balance Requests}{4}{subsubsection.3.1.1}
\contentsline {paragraph}{\numberline {3.1.1.1}For Single Accounts\\\\}{4}{paragraph.3.1.1.1}
\contentsline {paragraph}{\numberline {3.1.1.2}For Multiple Accounts\\\\}{4}{paragraph.3.1.1.2}
\contentsline {subsubsection}{\numberline {3.1.2}Topup Request}{5}{subsubsection.3.1.2}
\contentsline {subsubsection}{\numberline {3.1.3}New Account Registration-with validation}{6}{subsubsection.3.1.3}
\contentsline {subsubsection}{\numberline {3.1.4}New Account Registration-without validation}{7}{subsubsection.3.1.4}
\contentsline {subsubsection}{\numberline {3.1.5}Editing Account}{8}{subsubsection.3.1.5}
\contentsline {subsubsection}{\numberline {3.1.6}Adding Asset}{9}{subsubsection.3.1.6}
\contentsline {subsubsection}{\numberline {3.1.7}Editing Asset}{9}{subsubsection.3.1.7}
\contentsline {subsubsection}{\numberline {3.1.8}List Assets}{10}{subsubsection.3.1.8}
\contentsline {subsubsection}{\numberline {3.1.9}Set Customer Parameters}{10}{subsubsection.3.1.9}
\contentsline {subsubsection}{\numberline {3.1.10}Configure Customer Alerts}{11}{subsubsection.3.1.10}
\contentsline {subsubsection}{\numberline {3.1.11}Setting and Changing PIN}{12}{subsubsection.3.1.11}
\contentsline {subsubsection}{\numberline {3.1.12}Arrive Customer}{13}{subsubsection.3.1.12}
\contentsline {subsubsection}{\numberline {3.1.13}Vacate Customer}{13}{subsubsection.3.1.13}
\contentsline {subsubsection}{\numberline {3.1.14}Move Customer}{14}{subsubsection.3.1.14}
\contentsline {subsubsection}{\numberline {3.1.15}Turn ON Customer}{14}{subsubsection.3.1.15}
\contentsline {subsubsection}{\numberline {3.1.16}Customer socket state}{14}{subsubsection.3.1.16}
\contentsline {subsubsection}{\numberline {3.1.17}Request Customer Total Usage for Period}{15}{subsubsection.3.1.17}
\contentsline {subsubsection}{\numberline {3.1.18}Usage Reports}{15}{subsubsection.3.1.18}
\contentsline {subsubsection}{\numberline {3.1.19}Request Customer Detailed Statement for Period}{16}{subsubsection.3.1.19}
\contentsline {subsubsection}{\numberline {3.1.20}Transaction Reports}{16}{subsubsection.3.1.20}
\contentsline {subsubsection}{\numberline {3.1.21}Get Customer}{17}{subsubsection.3.1.21}
\contentsline {subsubsection}{\numberline {3.1.22}Search for Customers}{18}{subsubsection.3.1.22}
\contentsline {subsubsection}{\numberline {3.1.23}Get PIN}{18}{subsubsection.3.1.23}
\contentsline {subsubsection}{\numberline {3.1.24}Get Customer Parameters}{19}{subsubsection.3.1.24}
\contentsline {subsubsection}{\numberline {3.1.25}Remove Asset}{19}{subsubsection.3.1.25}
\contentsline {subsubsection}{\numberline {3.1.26}Remove Customer}{19}{subsubsection.3.1.26}
\contentsline {subsubsection}{\numberline {3.1.27}Get Service Types}{20}{subsubsection.3.1.27}
\contentsline {subsection}{\numberline {3.2}Site Specific Requests}{21}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}Request Site Structure}{21}{subsubsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.2}Request Site Structure\discretionary {-}{}{} Compact}{21}{subsubsection.3.2.2}
\contentsline {subsubsection}{\numberline {3.2.3}Request Site List}{22}{subsubsection.3.2.3}
\contentsline {subsubsection}{\numberline {3.2.4}Smart-search feature}{22}{subsubsection.3.2.4}
\contentsline {subsubsection}{\numberline {3.2.5}Request a Single Meter Reading}{23}{subsubsection.3.2.5}
\contentsline {subsubsection}{\numberline {3.2.6}Request Meter Readings for Site}{24}{subsubsection.3.2.6}
\contentsline {subsubsection}{\numberline {3.2.7}Request Missing Readings for Site}{24}{subsubsection.3.2.7}
\contentsline {subsubsection}{\numberline {3.2.8}Request Consumption for Period (by Site Item)}{25}{subsubsection.3.2.8}
\contentsline {subsubsection}{\numberline {3.2.9}Get Tariff}{26}{subsubsection.3.2.9}
\contentsline {subsubsection}{\numberline {3.2.10}Update Tariff}{26}{subsubsection.3.2.10}
\contentsline {subsubsection}{\numberline {3.2.11}Title Codes}{27}{subsubsection.3.2.11}
\contentsline {subsubsection}{\numberline {3.2.12}Mandatory Fields}{28}{subsubsection.3.2.12}
\contentsline {subsubsection}{\numberline {3.2.13}Software Revision}{28}{subsubsection.3.2.13}
\contentsline {subsubsection}{\numberline {3.2.14}Ping}{28}{subsubsection.3.2.14}
\contentsline {section}{\numberline {4}External Bill Processing}{29}{section.4}
\contentsline {subsection}{\numberline {4.1}Processing mechanism}{29}{subsection.4.1}
\contentsline {subsubsection}{\numberline {4.1.1}Check If Registered}{29}{subsubsection.4.1.1}
\contentsline {subsubsection}{\numberline {4.1.2}Send Bill}{29}{subsubsection.4.1.2}
\contentsline {subsubsection}{\numberline {4.1.3}Bill Payment}{31}{subsubsection.4.1.3}
\contentsline {section}{\numberline {5}Revision History}{33}{section.5}
\contentsline {section}{\numberline {6}Appendix A}{35}{section.6}
\contentsline {subsection}{4.1 Introducing JSON}{35}{section*.1}
